CELERY:
Celery is a task queue implementation for Python web applications used to 
asynchronously execute work outside the HTTP request-response cycle. 
Celery is an implementation of the task queue concept. 

Brokers/Message broker: Celery uses broker to pass message between django project and celery worker.

Current supporting brokers:
    1) RabbitMQ
    2) Redis
    3) Amazon SQS


1) Why Celery ?

    a)  Web app is all about working with request and response. When the url is accessed
        of certain web application a request is sent to the server. Django receives the 
        request and returns the response needed. But before sending response back it
        performs/execute database query, processing data which might take time sometimes.
        Now the user have to wait for the response which might or might not take much time.
        The request and response cycle should be fast otherwise we would leave the user
        waiting for way too long.

    b)  It is also very useful in scheduling the task. If we want to run specific task at 
        certain period of time.

2) What happens if the process is slow ?

Our web server can only serve a certain number of users at a time. so if the 
process is slow it limits the amount of pages that your application can serve at a time.

Why not use cache to solve this problem ?

Yes caching and optimizing database query can help to solve the problem but this is
not an option when exporting big amount of data, video/images processing. So in such case
we need to use celery.


To schedule particular task we use celery beat.

Celery beat crontab
crontab(): 
    Execute every minute.

crontab(minute=0, hour=0):
    Execute daily at midnight.

crontab(minute=0, hour='*/3'):
    Execute every three hours: midnight, 3am, 6am, 9am, noon, 3pm, 6pm, 9pm.

crontab(minute=0, hour='0,3,6,9,12,15,18,21'):
    Same as previous.

crontab(minute='*/15'):
    Execute every 15 minutes.

crontab(day_of_week='sunday'):
    Execute every minute (!) at Sundays.

crontab(minute='*', hour='*', day_of_week='sun'):
    Same as previous.

crontab(minute='*/10',hour='3,17,22', day_of_week='thu,fri'):
    Execute every ten minutes, but only between 3-4 am, 5-6 pm, 
    and 10-11 pm on Thursdays or Fridays.

crontab(minute=0, hour='*/2,*/3'):
    Execute every even hour, and every hour divisible by three. 
    This means: at every hour except: 1am, 5am, 7am, 11am, 1pm, 
    5pm, 7pm, 11pm

crontab(minute=0, hour='*/5'):
    Execute hour divisible by 5. This means that it is triggered 
    at 3pm, not 5pm (since 3pm equals the 24-hour clock value of “15”, 
    which is divisible by 5).

crontab(minute=0, hour='*/3,8-17'):
    Execute every hour divisible by 3, and every hour during office hours (8am-5pm).

crontab(0, 0, day_of_month='2'):
    Execute on the second day of every month.

crontab(0, 0, day_of_month='2-30/2'):
    Execute on every even numbered day.

crontab(0, 0, day_of_month='1-7,15-21'):
    Execute on the first and third weeks of the month.

crontab(0, 0, day_of_month='11',month_of_year='5'):
    Execute on the eleventh of May every year.

crontab(0, 0, month_of_year='*/3'):
    Execute every day on the first month of every quarter.



# To run celery worker:
    celery -A <project_name> worker -l info
# To run celery beats:
    celery -A <project_name> beat -l info