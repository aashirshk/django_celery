from django.urls import path
from . import views

urlpatterns = [
    path('', views.createRandomUser.as_view(), name="create_random_user")
]