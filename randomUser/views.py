from django.shortcuts import render
from rest_framework.views import APIView, Response
from .tasks import create_random_user
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import string
import time
# Create your views here.

class createRandomUser(APIView):
    def post(self, request):
        total = request.data['total']
        create_random_user.delay(total)
        print("Hello")
        # for t in range(int(total)):
        #     username = f'user_{get_random_string(10, string.ascii_letters)}'
        #     email = f'{username}@email.com'
        #     password = get_random_string(10)
        #     User.objects.create(username=username, email=email, password=password)
        # finish = time.perf_counter()

        # print(f"Finished at {round(finish - start, 2)}") 
        return Response({'success': True})

# def createRandomUser(request):
#     print("Function view")
#     total = request.data['total']
#     create_random_user.delay(total)
#     for t in range(int(total)):
#         username = f'user_{get_random_string(10, string.ascii_letters)}'
#         email = f'{username}@email.com'
#         password = get_random_string(10)
#         User.objects.create(username=username, email=email, password=password)
#     return None
