from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
import string
from celery.schedules import crontab
from celery import shared_task
import time
# @shared_task()
# def create_random_user(total):
#     for t in range(int(total)):
#         username = f'user_{get_random_string(10, string.ascii_letters)}'
#         email = f'{username}@email.com'
#         password = get_random_string(10)
#         User.objects.create(username=username, email=email, password=password)
#     return '{} random users created with success!'.format(total)

# shared task

@shared_task
def create_random_user(total):
    # print("Creating task")
    # return "Creating task..."
    time.sleep(5)
    for i in range(int(total)):
        username = 'user_{}'.format(
            get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        password = get_random_string(50)
        User.objects.create_user(
            username=username, email=email, password=password)
    return '{} random users created with success!'.format(total)
