import os
from celery import Celery
from .env_vars import env_vars

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celery_dj.settings')
app = Celery(f'{env_vars.app_name}')
#Configuration from setting file but variable name statrting from CELERY i.e. the name space.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))