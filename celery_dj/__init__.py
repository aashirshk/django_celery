from .celery import app as celery_app

# To ensure that the Celery app is loaded when Django starts,
__all__ = ['celery_app']
