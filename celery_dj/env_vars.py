import os
from typing import List
from pydantic import BaseSettings


class EnvVars(BaseSettings):
    django_secret_key: str = ""
    django_debug: bool = False
    django_allowed_host: List = ['*']

    app_name: str = ""

    ###Celery configuration########
    celery_broker_url: str = ''
    celery_result_backend: str = ''
    celery_accept_content: List = ['application/json']
    celery_task_serializer: str = ''
    celery_result_serializer: str = ''
    celery_timezone: str = ''

    ###Database configuration#########
    database_name: str = ""
    database_host: str = ""
    database_port: str = ""
    database_user: str = ""
    database_password: str = ""

if os.path.exists(".env"):
    env_vars = EnvVars(_env_file=".env", _env_file_encoding="utf-8")
else:
    print("Env file does not exist.")

